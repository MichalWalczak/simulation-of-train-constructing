#include <string.h>
#include "class_cargo.h"

/*==============================================================================
						constructor
===============================================================================*/

cargo::cargo(int type, int wag_weight,const char *wag_name,const char *car_name, 
			 int car_weight ):
cargo_weight(car_weight), wagon(wag_name, wag_weight, type)						
{
	strcpy(cargo_name, car_name);
}
/*==============================================================================
						destructor
===============================================================================*/
cargo::~cargo()
{
    //wagon_count--;
    cout << "nie ma pochodnej" << endl;
    //delete this; ????
}

/*==============================================================================
						methodes
===============================================================================*/
void cargo::show_stats() const{
	cout << name <<"\tweight: "<< weight<< "\tcargo : " 
	<< cargo_name<<"\t cargo weight: " << cargo_weight << endl;
}
//==============================================================================
int cargo::get_weight() const {
    return weight+cargo_weight;
}
