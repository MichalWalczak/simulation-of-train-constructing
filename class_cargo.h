
#ifndef _WAGON_
#define _WAGON_
#include "class_wagon.h"
#endif//_WAGON_
class cargo : public wagon
{
	private:
        char secret_cargo[NAME_SIZE];// not usable yet
        int secret_cargo_value; 	 // not usable yet
    public:
		cargo(int type, int wag_weight,const char *wag_name, 
										const char *car_name, int car_weight );
		~cargo();										
	    char cargo_name[NAME_SIZE];
		int cargo_weight;
	    void show_stats() const;
	    int get_weight() const;
};

