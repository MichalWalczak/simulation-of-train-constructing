#include "class_coach.h"

/*==============================================================================
						constructor
===============================================================================*/
coach::coach(int type, int wag_weight,const char *wag_name, 
											int seats , int o_seats):
seats_count(seats),ocupied_seats(o_seats), wagon(wag_name, wag_weight, type)											
{
}
/*==============================================================================
						methodes
===============================================================================*/
void coach::show_stats() const{
    cout<<name<<"\tweight: "<<weight<<"\t"<<"number of seats: "
	<<ocupied_seats<<"/"<<seats_count<< endl;
}
//==============================================================================
int coach::get_weight() const{
    return weight+ocupied_seats*AV_WEIGHT;
}

