#ifndef _WAGON_
#define _WAGON_
#include "class_wagon.h"
#endif //_WAGON_

#define AV_WEIGHT 80

class coach : public wagon
{
    public:
		coach(int type, int wag_weight,const  char *wag_name, int seats , int o_seats);
	    int seats_count, ocupied_seats;
	    
	    void show_stats() const;
	    int get_weight() const;
};
