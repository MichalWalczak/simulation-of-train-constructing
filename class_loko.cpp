#include "class_loko.h"

/*==============================================================================
						constructor
===============================================================================*/

locomotive::locomotive(int type, int wag_weight,const char *wag_name, 
													  int lok_power ):
power(lok_power), wagon(wag_name, wag_weight, type)													
{
}
/*==============================================================================
						methodes
===============================================================================*/
void locomotive::show_stats() const{
    cout << name<<"\tweight: " << weight 
	<<"\tpower : " << power << endl;
}
//==============================================================================
int locomotive::get_power() const{
    return power;
}
