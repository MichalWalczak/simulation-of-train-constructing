#ifndef _WAGON_
#define _WAGON_
#include "class_wagon.h"
#endif //_WAGON_


class locomotive : public wagon {
    public:
		locomotive(int type, int wag_weight,const char *wag_name, int lok_power );
	    int power;
	    void show_stats() const;
	    int get_power() const;
};
