#include "class_repaircar.h"

/*==============================================================================
						constructor
===============================================================================*/
repaircar::repaircar(int type, int wag_weight,const char *wag_name, 
				   const char *rapair_cat, const char *tools, int tools_w):
tools_weight(tools_w), wagon(wag_name, wag_weight, type)											
{
	strcpy(repair_category, rapair_cat);
	strcpy(tools_type, tools);
}
/*==============================================================================
						methodes
===============================================================================*/
void repaircar::show_stats() const{
    cout<<name<<"\tweight: "<<weight<<"\t"<<"repair category: "
	<<repair_category<<" tools: "<<tools_type<<" tools weight: "
	<<tools_weight<<endl;
}
//==============================================================================
int repaircar::get_weight() const{
    return weight+tools_weight;
}

