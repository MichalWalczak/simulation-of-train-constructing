#ifndef _WAGON_
#define _WAGON_
#include "class_wagon.h"
#endif //_WAGON_

class repaircar : public wagon
{
    public:
		repaircar(int type, int wag_weight,const char *wag_name, 
				   const char *rapair_cat, const char *tools, int tools_w);
	    int tools_weight;
		char repair_category[NAME_SIZE], tools_type[NAME_SIZE];
	    
	    void show_stats() const;
	    int get_weight() const;
};
