#include "class_train.h"
#include "class_cargo.h"
#include "class_loko.h"
#include "class_coach.h"
/*==============================================================================
 						fun to watch
===============================================================================*/
// just for fun -> not essential -> dont try to read -> dont try to understand
#define L_UP cout <<" "<<(char)203<<"  "<<(char)201<<(char)205<<(char)205<<(char)187;
#define L_MID_UP cout <<(char)201<<(char)202<<(char)205<<(char)205<<(char)188<<"  "<<(char)186;
#define L_MID_DOWN cout <<(char)185<<"      "<<(char)204;
#define L_DOWN cout <<(char)200<<"Oo"<<(char)205<<(char)205<<"Oo"<<(char)188;
#define L_DOWN_M cout <<(char)200<<"oO"<<(char)205<<(char)205<<"oO"<<(char)188;

#define P cout << "\n";

#define C_UP cout <<" "<<(char)201<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)187;
#define C_MID_UP cout <<" "<<(char)186<<" "<<(char)201<<(char)187<<" "<<(char)201<<(char)187<<" "<<(char)201<<(char)187<<" "<<(char)186;
#define C_MID_DOWN cout <<(char)205<<(char)185<<" "<<(char)200<<(char)188<<" "<<(char)200<<(char)188<<" "<<(char)200<<(char)188<<" "<<(char)204;
#define C_DOWN 	cout <<" "<<(char)200<<"OO"<<(char)205<<(char)205<<"OO"<<(char)205<<(char)205<<"OO"<<(char)188;
#define C_DOWN_M 	cout <<" "<<(char)200<<"oo"<<(char)205<<(char)205<<"oo"<<(char)205<<(char)205<<"oo"<<(char)188;

#define CA_UP cout <<" "<<(char)201<<(char)187<<"        "<<(char)201<<(char)187;
#define CA_MID_UP cout <<" "<<(char)186<<(char)186<<"        "<<(char)186<<(char)186;
#define CA_MID_DOWN cout <<(char)205<<(char)185<<(char)200<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)205<<(char)188<<(char)204;
#define CA_DOWN cout <<" "<<(char)200<<"OO"<<(char)205<<(char)205<<"OO"<<(char)205<<(char)205<<"OO"<<(char)188;
#define CA_DOWN_M cout <<" "<<(char)200<<"oo"<<(char)205<<(char)205<<"oo"<<(char)205<<(char)205<<"oo"<<(char)188;

#define G 9.81 //acceleration due to gravity
#define F 0.004 //friction ratio
#define CHANGE_UNITS 3.6 // m/s to Km/h change ratio
#define KILO 1000

using namespace std;

/*==============================================================================
						methodes
==============================================================================*/
float train::get_train_speed()
{
	float speed=0;
	long train_weigth=0, train_power=0;
	double train_weigth_in_newtons;  
	
	train_weigth = this->get_train_weight();
	train_power = this->get_train_power();
	cout <<"\ntrain weight " << train_weigth << " combined power: " 
	<< train_power <<" kW " << endl;
	train_weigth_in_newtons = train_weigth * G;
	speed = (((train_power*KILO)/(train_weigth_in_newtons*F))*CHANGE_UNITS);
	cout <<"\nspeed " <<  speed <<" km/h "<<endl;
	return speed;
}
//==============================================================================
int train::get_train_power()
{
	int train_power=0;
	wagon *iterator;
	
	iterator=this->head;
	while(iterator!=NULL)
	{
		train_power += iterator->get_power();
		iterator = iterator->next;
	}
	return train_power;
}
//==============================================================================
int train::get_train_weight()
{
	wagon *iterator;
	int train_weight=0;
	iterator=this->head;
	while(iterator!=NULL)
	{
		train_weight += iterator->get_weight();
		iterator = iterator->next;
	}
	return train_weight;	
}
//==============================================================================
int train::add_wagon(int type, int weight, const char name[], const char car_name[], 
					 int arg1, int arg2)
{
	wagon *new_wagon, *tmp;
	if(this->head==NULL){ // 3 types of defined wagons loco,cargo, coach
		if(type==1)   
			this->head=new locomotive(type, weight,name, arg1 );
		else if(type==2)
			this->head=new coach(type, weight, name, arg1, arg2);
		else if(type==3)
			this->head=new cargo(type, weight, name, car_name, arg1);
			
		this->head->prev=NULL;
	    this->head->next=NULL;
	}
	else{
		tmp =this->head;
		while(tmp->next!=NULL)
    	    tmp=tmp->next;

		if(type==1)
			new_wagon=new locomotive(type, weight, name, arg1 );
		else if(type==2)
			new_wagon=new coach(type, weight, name, arg1, arg2);
		else if(type==3)
			new_wagon=new cargo(type, weight, name, car_name, arg1);

		else{
			throw "invalid wagon type";
			return 0;
		}			
		new_wagon->prev=tmp;
		tmp->next=new_wagon;
		new_wagon->next=NULL;
	}
	return 1;
}
//==============================================================================
int train::add_wagon(wagon *wagon_to_add)
{
	wagon *tmp;
	if(this->head==NULL){ 
		this->head=wagon_to_add;
			
		this->head->prev=NULL;
	    this->head->next=NULL;
	}
	else{
		tmp =this->head;
		while(tmp->next!=NULL)
    	    tmp=tmp->next;
				
		wagon_to_add->prev=tmp;
		tmp->next=wagon_to_add;
		wagon_to_add->next=NULL;
	}
	return 1;
}
//==============================================================================
int train::rmv_wagon(int which_one)
{
	wagon *new_wagon, *tmp;
	int i;
	if(this->head==NULL){
		throw "there is no train"; 
		return 0;	
	}
	else if((which_one<=wagon::wagon_count) && (which_one>0)){
		tmp = this->head;
		
		for(i=1;i<which_one;i++)
    	    tmp=tmp->next;

        if(tmp==this->head){
    		this->head = tmp->next;
    		delete tmp;
    	}                                          
    	else{
	    	if (tmp->next != NULL){
	    		tmp->next->prev = tmp->prev;
			}
			tmp->prev->next = tmp->next;	
	    	delete tmp;
	    }
	}
	else{
	    throw "invalid wagon";
	    return 0;
	}
    return 1;
}
//==============================================================================
int train::enter_wagon_stats()
{
	char name[NAME_SIZE], car_name[NAME_SIZE];
	int type=0, power=0, car_weight=0, seats=0, o_seats=0, weight ; 
	
	cout <<"enter wagon type:\nlocomotive: 1\ncoach: 2\ncargo: 3" << endl;
	cin >> type;
	cout << "enter wagon name :" << endl;
	cin.width(sizeof(name));
	cin >> name;
	cout << "enter wagon weight :" << endl;
	cin >> weight;
	if (type==1){
		cout << "enter locomotive power :" << endl;
		cin >> power;
		this->add_wagon(type, weight, name, car_name, power, 0);
	}
	else if(type==2){
		cout << "enter number of seats in coach :" << endl;
		cin >> seats;
		cout << "enter number of ocupied seats in coach :" << endl;
		cin >> o_seats;
		this->add_wagon(type, weight, name, car_name, seats, o_seats);
	}
	else if(type==3){
		cout << "enter cargo name :" << endl;
		cin.width(sizeof(car_name));
	    cin >> car_name;
	    cout << "enter cargo weight :" << endl;
	    cin >> car_weight;
		this->add_wagon(type, weight, name, car_name, car_weight, 0);
	}
}
//==============================================================================
int train::sort_train()
{
    wagon *first_wagon = NULL, *current= NULL, *iterator= NULL;
	current = this->head;
    while (current != NULL)
    {
		iterator = current;
		first_wagon = iterator;
		while(iterator->next != NULL)
		{
			iterator = iterator->next;
			if(first_wagon->wagon_type>iterator->wagon_type){
				first_wagon = iterator;
			}
		}
		if (first_wagon != current) {//take wagon & connect neighburs
			first_wagon->prev->next = first_wagon->next;
			if (first_wagon->next){
				first_wagon->next->prev = first_wagon->prev;	
			}			
			first_wagon->prev = current->prev;//put first_wagon before current
			
			if (current->prev){//point last min value to new min value
				current->prev->next = first_wagon;
			}			
			first_wagon->next = current;//current is next for first_wagon			
			current->prev = first_wagon;
		} 
		else {
			current = current->next;
		}
    }    
    while (this->head->prev) {//go to begiining of the train
		this->head = this->head->prev;
    }
}
//==============================================================================
int train::show_train()
{
	wagon *iterator;
	iterator=this->head;
	while(iterator!=NULL)
	{
		iterator->show_stats();
		iterator = iterator->next;
	}
	return 1;
}
//==============================================================================
int train::print_train()
{
	int i=0;
	wagon *iterator;
	iterator=this->head;

	while((iterator!=NULL) && (i<6))
	{
		if(iterator->wagon_type==1) {L_UP}
		if(iterator->wagon_type==2) {C_UP}
		if(iterator->wagon_type==3) {CA_UP}		
		iterator = iterator->next; i++;
	}
	iterator=this->head;i=0; P
	while((iterator!=NULL) && (i<6))
	{
		if(iterator->wagon_type==1) {L_MID_UP}
		if(iterator->wagon_type==2) {C_MID_UP}
		if(iterator->wagon_type==3) {CA_MID_UP}		
		iterator = iterator->next;i++;
	}
	iterator=this->head;i=0; P
	while((iterator!=NULL) && (i<6))
	{
		if(iterator->wagon_type==1) {L_MID_DOWN}
		if(iterator->wagon_type==2) {C_MID_DOWN}
		if(iterator->wagon_type==3) {CA_MID_DOWN}		
		iterator = iterator->next;i++;
	}
	iterator=this->head;i=0; P
	while((iterator!=NULL) && (i<6))
	{
		if(iterator->wagon_type==1) {L_DOWN}
		if(iterator->wagon_type==2) {C_DOWN}
		if(iterator->wagon_type==3) {CA_DOWN}		
		iterator = iterator->next;i++;
	}
	return 1;
}
