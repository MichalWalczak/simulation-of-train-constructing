#include <iostream>
#ifndef _WAGON_
#define _WAGON_
#include "class_wagon.h"
#endif //_WAGON_

class train{		 
public:
    class wagon *head;
    int get_train_power();
    int get_train_weight();
    int show_train();
    float get_train_speed();
    int add_wagon(int type, int weight, const char name[],
				  const char car_name[],int arg1, int arg2);
	int add_wagon(wagon *);
    int rmv_wagon(int);
    int enter_wagon_stats();
    int sort_train();
    int print_train();
};
//==============================================================================
