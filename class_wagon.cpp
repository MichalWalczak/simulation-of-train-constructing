#include "class_wagon.h"


/*==============================================================================
						constructor
==============================================================================*/
wagon::wagon()
{
	wagon_count++;
}
//==============================================================================
wagon::wagon(const char *wag_name, int wag_weight, int type):
weight(wag_weight), wagon_type(type)
{
	strcpy(name, wag_name);
	wagon_count++;
}
/*==============================================================================
						destructor
==============================================================================*/
wagon::~wagon()
{
    wagon_count--;
    cout << "nie ma bazy" << endl;
    //delete this; ????
}
/*==============================================================================
						static wariable
==============================================================================*/
int wagon::wagon_count=0;

/*==============================================================================
						methodes
==============================================================================*/
void wagon::show_stats() const
{
	    cout << name << weight << endl;
}
//==============================================================================
int wagon::get_weight() const
{
	return weight;
}
//==============================================================================
int wagon::get_power() const
{
	return 0; //normal wagon has no power
}
//==============================================================================
/*
int connect_wagon(wagon * wagon_to_connect)
{
	wagon *iterator;
	if(first == NULL)
		first = this
	else{
		iterator=first;
		while(iterator!=NULL)
			iterator = iterator->next;
			
		iterator->next=this;
		this->next=NULL;
	}
	return 1;	
}*/


