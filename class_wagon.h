#include <iostream>
#include <string.h>
#define NAME_SIZE 20

using namespace std;

class wagon{			  
public:
    wagon();
    wagon(const char[], int, int);
	virtual ~wagon();  
    char name[NAME_SIZE];
    int weight, wagon_type; 
	static int wagon_count ;
    class wagon  *next, *prev, *first;
	
	//int connect_wagon();
    void virtual show_stats() const;
	int virtual get_weight() const;
	int virtual get_power() const;
};
//==============================================================================
