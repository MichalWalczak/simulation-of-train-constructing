#include<stdio.h>
#include<windows.h>
#include <cstdlib>
#include "class_train.h"
#include "class_cargo.h"
#include "class_repaircar.h"
using namespace std;

void menu();
void quick_add_wagon(train &);

int main(int argc, char *argv[])
{
	int read_success=0, menu_option=0,which_one=0;
	train train1;
	train1.head=NULL;

	do{
		try{
			menu();
			read_success=scanf("%1d",&menu_option);
			if(read_success!=0){
				switch (menu_option){
					case 1:{
						quick_add_wagon(train1);
						cout <<"\nnr of wagons in train "<< wagon::wagon_count;					
					} break;
					case 2:{
						cout <<"\t2 - which wagon do you want to remove \n";
						cin>>which_one;
						train1.rmv_wagon(which_one);
					}break;
					case 3:	{train1.show_train();} break;	
					case 4:	{train1.get_train_speed();} break;
					case 5:	{train1.sort_train();} break;
					case 6:	{train1.print_train();} break;
					case 7:	{cout <<"\t7 - end program\n\n\t";}	break;
					default: break;	 
				}
			}
			else{
				printf("\t invalid entry\n");
				fflush(stdin);
			}	
			}
			catch(char const *s){
			cout<<"Error: "<<s<<endl;
			}
	}while(menu_option!=7);	
	return 0;
}

void menu()
{
	cout << "\n\n\t\tMAIN MENU\n\n";	
	cout <<"\t1 - enter_wagon_stats \n";
	cout <<"\t2 - rmv_wagon \n";
	cout <<"\t3 - show_train \n";
	cout <<"\t4 - get_train_speed \n";
	cout <<"\t5 - sort_train\n";
	cout <<"\t6 - print_train \n";
	cout <<"\t7 - end program\n\n\t";
}
void quick_add_wagon(train &train1)
{
    train1.add_wagon(1, 40000, "loko2 ", "", 400, 0);
	train1.add_wagon(2, 10000, "coach2 ", "", 80, 60);
	train1.add_wagon(1, 40000, "loko1 ", "", 400, 0);
	train1.add_wagon(3, 20000, "cargo1 ", "oil", 40000, 0);
	train1.add_wagon(3, 20000, "cargo2 ", "coal", 40000, 0);
	train1.add_wagon(2, 10000, "coach1 ", "", 80, 75);
}
/*	cargo cargo3(0, 5000, "NEWcar", "coco", 50000);
	cargo *ptr_to_NEWcargo = &cargo3;
	repaircar repair3(0, 50000, "NEWrep", "railrep", "hammers & nails", 300);
	repaircar *ptr_to_NEWrepaircar = &repair3;
*/
	//train1.add_wagon(ptr_to_NEWcargo);
	//train1.add_wagon(ptr_to_NEWrepaircar);
	//train1.enter_wagon_stats();
